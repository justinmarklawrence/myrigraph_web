import { BrowserRouter, Routes, Route } from "react-router-dom";
import GraphPage from "../Graph";
import AdminPage from "../Admin";
// import RegisterPage from "../register";

export default function Application() {
    return (
        <BrowserRouter>
        <Routes>
          {/* <Route path="register" element={<RegisterPage />} /> */}
          <Route path="/" element={<GraphPage />} />
          <Route path="admin" element={<AdminPage />} />
          <Route path="graph" element={<GraphPage />} />
        </Routes>
      </BrowserRouter>
    )
}