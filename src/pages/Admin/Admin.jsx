import { useEffect, useState } from "react";
import AsyncCreatableSelect from 'react-select/async-creatable';
import { addNode, getNodes, getNodesByName } from "../../services/nodes";

export default function Admin() {
  const [nodes, setNodes] = useState({});
  const [newNodeName, setNewNodeName] = useState({value: "", label: ""});

  useEffect(() => {
    getNodes(setNodes);
}, []);

console.log({nodes});
  const handleTagChange = (newValue, a, b) => {
      console.log('tag change', {newValue, a, b});
    const inputValue = newValue.replace(/\W/g, '');
    setNewNodeName(inputValue);
  };

  const handleNewName = (newName) => {
    setNewNodeName({label:newName, value:""});
};

  const loadOptions = (
    inputValue,
    callback
  ) => {
    getNodesByName(inputValue, callback);
  };

  return (
    <div>
      <h1>Admin</h1>
        <AsyncCreatableSelect 
            cacheOptions
            onCreateOption={handleNewName}
            value={newNodeName}
            placeholder="Node name ..."
            loadOptions={loadOptions}
            onInputChange={handleTagChange}
        />
      
      <button onClick={() => {
          console.log("ADDING NODE:", newNodeName);
          addNode({name: newNodeName}, (newNodes) => {
            console.log("DONE!");
            setNodes(newNodes);
          });
          setNewNodeName("");
      }}>Add node</button>
      <h2>Nodes</h2>
      {nodes &&
        nodes.length > 0 &&
        nodes.map((node) => {
          return <div key={node.key}>Node: {node.name}</div>;
        })}
    </div>
  );
}
