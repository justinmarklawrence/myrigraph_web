import firebase from "firebase/app";
import "firebase/database";
import NodeSchema from "../config/schema/node";
import { nanoid } from "nanoid";

let cache = [];

export function getNodes(callback, config) {
  let options = config || {};
  const { hardRefresh = false } = options;
  const ref = firebase.database().ref(`nodes/`);

  if (hardRefresh || cache.length === 0) {
    console.log("HITTING DB!");
    ref.get().then((snapshot) => {
      cache = Object.entries(snapshot.toJSON()).map(([key, value]) => ({
        key,
        ...value,
      }));
    });
    callback(cache);
  } else {
    callback(cache);
  }
}

export function getNodesByName(str, callback) {
  getNodes((nodes) => {
    callback(
      nodes
        .filter((node) => node.name.toLowerCase().includes(str.toLowerCase()))
        .map((node) => ({ value: node.key, label: node.name }))
    );
  });
}

export function addNode(newNode, callback) {
  const nodeUUID = nanoid();
  const nodeWithUUID = {
    uuid: nodeUUID,
    createdOn: new Date().toString(),
    ...newNode,
  };
  const { children = [], parents = [], tags = [] } = nodeWithUUID;

  delete nodeWithUUID.children;
  delete nodeWithUUID.parents;
  delete nodeWithUUID.tags;

  // Add root node
  NodeSchema.isValid(nodeWithUUID).then((isValid) => {
    if (isValid) {
      const rootRef = firebase.database().ref(`nodes/${nodeUUID}`);
      const relRef = firebase.database().ref(`rels/${nanoid()}`);
      const tagRef = firebase.database().ref(`nodes/${nanoid()}`);

      rootRef.get().then(() => {
        rootRef.set(nodeWithUUID);
        getNodes(callback, { hardRefresh: true });
      });

      // Add children
      children.forEach((childUUID) => {
        relRef.set({
          uuid: nanoid(),
          parent: nodeUUID,
          child: childUUID,
        });
      });

      // Add parents
      parents.forEach((parentUUID) => {
        relRef.set({
          uuid: nanoid(),
          parent: parentUUID,
          child: nodeUUID,
        });
      });

      // Add tags
      tags.forEach((tagUUID) => {
        tagRef.set({
          uuid: nanoid(),
          initiator: nodeUUID,
          reciever: tagUUID,
        });
      });
    } else {
      console.warn("Tried to add invalid node:", nodeWithUUID);
      return false;
    }
  });
}
