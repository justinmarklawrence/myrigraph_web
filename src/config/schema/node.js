import * as yup from "yup";

const schema = yup.object().shape({
  uuid: yup.string().required(),
  name: yup.string().required().min(1),
  createdOn: yup.string().required(),
});

export default schema;
